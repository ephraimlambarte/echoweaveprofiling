<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PutExpDateOnAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
      Schema::table('prof_licenses', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('physical_exams', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('nur_bag_tec_comps', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('liability_insurances', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('hand_washing_competencies', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('flu_vaccines', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('drivers_licenses', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('cprs', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('competency_checklists', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('chest_xrays', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('annual_evaluations', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
      Schema::table('auto_insurances', function (Blueprint $table) {
          $table->date('startDate');
          $table->integer('userID')->unsigned();
          $table->foreign('userID')->references('id')->on('employees');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
