@extends('main')
@section('stylesheet')
  <style>
    #container{
      position: absolute;
      bottom:0;
      top:0;
      left:0;
      right:0;
    }
    #error{
      font-size: 36px;
      padding: 20px;
      font-family: Helvetica;
    }

  </style>

@stop
@section('content')
  <div id = 'container' style = "background: url('{{asset('../../images/background'.rand(1,4).'.jpg')}}') no-repeat center center fixed;
                                -webkit-background-size: cover;
                                -moz-background-size: cover;
                                -o-background-size: cover;
                                background-size: cover;">

          <div class='flexContainerCenter'>
            <div id = 'error'>
              <strong>Error: </strong>{{$error}}
            </div>
          </div>
  </div>
@stop
