@extends('admin.auth.adminAuthTemplate')

@section('title', 'Admin Login')

@section('css')
  <link href="{{asset('css/admin/adminLogin.css')}}" rel="stylesheet">
@stop

@section('content')
  <div class = 'flexContainerMiddle' >

    <div class ='regAdminLoginForm'>
      @include('Partials._message')
      <div><center><strong>Admin-Login</strong></center></div>


      {!! Form::open(['route' => 'login.admin.verify',
              'data-parsley-validate'=>'']) !!}
        {{Form:: label('email', 'Email:')}}
        {{Form:: text('email', null, array(
                          'placeholder'=>'Username',
                          'class'=>'form-control',
                          'required'=>'',
                          'maxlength'=>'100'))}}
        {{Form:: label('password', 'Password:')}}
        {{Form:: password('password', array(
                          'placeholder'=>'Password',
                          'class'=>'form-control',
                          'required'=>'',
                          'maxlength'=>'100'))}}
        <div class='genericDivTop'>

        {{Form::checkbox('remember_me')}}
        {{Form:: label('remember_me', 'Remember me')}}
        </div>
        {{Form::submit('Login', array('class'=>'btn btn-success btn-block'))}}
      {!! Form::close() !!}
      <div>
        <div class='row'>
          <!--div class='col-6'>
            {{Html::link('#', 'Forgot Password', array('class'=>'btn btn-block btn-info genericDivTop'
                                                      ))}}
          </div-->
          <div class='col-12'>
            <a href ='{{route('register.admin')}}' class='btn btn-block btn-primary genericDivTop'>Register</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class='loginFooter'>
    <center>
      <img src= '{{asset('images/companylogo.png')}}' id = 'myimage'><strong>Powered By:</strong> EchoWeave Tech Solutions
    </center>
  </div>
@stop

@section('scripts')
  <script src= '{{asset('js/admin/adminLogin.js')}}'></script>
@stop
