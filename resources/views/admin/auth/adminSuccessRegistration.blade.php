@extends('admin.auth.adminAuthTemplate')
@section('title', 'Admin Registration')

@section('css')
<link href="{{asset('css/admin/adminSuccessRegistration.css')}}" rel="stylesheet">
@stop
@section('content')
		<div class = 'flexContainerMiddle' >
			<div class ='regAdminLoginForm'>
				@include('Partials._message')
				<div id = 'adminInfo'>
					<div class='genericDivBottom'>
						<strong>Username:</strong>
					</div>
					<div class='genericDivBottom'>
						<strong>email:</strong>
					</div>
					<div  class='genericDivBottom'>
						{{$userName}}
					</div>
					<div  class='genericDivBottom'>
						{{$email}}
					</div>
				</div>
				<div>
					<a class='btn btn-success' href = "{{route('login.admin')}}">Go to login page</a>

				</div>
			</div>
		</div>
@stop
