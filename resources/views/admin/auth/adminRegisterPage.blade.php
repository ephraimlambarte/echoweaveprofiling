@extends('admin.auth.adminAuthTemplate')

@section('title', 'Admin Login')

@section('css')
  <link href="{{asset('css/admin/adminRegister.css')}}" rel="stylesheet">
@stop

@section('content')
  <div class = 'flexContainerMiddle' >

    <div class ='regAdminLoginForm'>
      <div><center><strong>Admin-Register</strong></center></div>
      @include('Partials._message')
      {!! Form::open(['route' => 'register.post.admin',
              'data-parsley-validate'=>'',
              'files'=>true]) !!}
        <div>
          <center>
            <img src="{{asset('images/defaultimage.png')}}" id = 'profileImage' class="genericImageCircle" alt="Cinque Terre">
          </center>
        </div>
        <div>
        {{Form::label('uploadProfileImage', 'Upload Profile Picture:')}}
        {{Form::file('uploadProfileImage', array('accept'=>
          '.png,.jpg'
        ))}}
      </div>
        {{Form:: label('userName', 'Username:')}}
        {{Form:: text('userName', null, array(
                          'placeholder'=>'Username',
                          'class'=>'form-control',
                          'required'=>'',
                          'maxlength'=>'100'))}}
        {{Form:: label('password', 'Password:')}}
        {{Form:: password('password', array(
                          'placeholder'=>'Password',
                          'class'=>'form-control',
                          'required'=>'',
                          'maxlength'=>'100',
                          'data-parsley-equalto'=>"#password_confirmation"))}}
        {{Form:: label('password_confirmation', 'Confirm Password:')}}
        {{Form:: password('password_confirmation', array(
                          'placeholder'=>'Confirm Password',
                          'class'=>'form-control',
                          'required'=>'',
                          'maxlength'=>'100',
                          'data-parsley-equalto'=>"#password"))}}
        {{Form:: label('adminKey', 'Admin Key:')}}
        {{Form:: password('adminKey', array(
                          'placeholder'=>'Confirm Password',
                          'class'=>'form-control',
                          'required'=>'',
                          'maxlength'=>'100'))}}
        {{Form:: label('email', 'Email:')}}
        {{Form:: email('email', null, array(
                          'placeholder'=>'Email',
                          'class'=>'form-control',
                          'required'=>'',
                          'maxlength'=>'100',
                          'data-parsley-type='=>"email"))}}

        {{Form::submit('Register Admin', array('class'=>'btn btn-success btn-block',
                            'style'=>'margin-top:20px;'))}}
      {!! Form::close() !!}
    </div>
  </div>
@stop

@section('scripts')
  <script src= '{{asset('js/admin/adminRegister.js')}}'></script>

@stop
