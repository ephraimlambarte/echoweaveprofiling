@extends('admin.auth.adminAuthTemplate')

@section('title', 'Admin Verification')
@section('css').
    <link href="{{asset('css/admin/adminVerificationEmail.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class = 'flexContainerMiddle' >
    <div class ='regAdminLoginForm'>
      <strong>To complete registration please click the link we sent to your email</strong>
    </div>
  </div>
@stop
