@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/statistics.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    <div class='row'>
			<div class='col-md-12'>

					<button id = 'generateCsv' class='btn btn-info btn-lg'>Generate CSV File</button>


					<a id = 'downloadCsv'>Download CSV</a>

				<table class='table talbe-striped'>
					<thead>
						<tr>
              <th>Title</th>
              <th>Date Of Hire</th>
							<th>Name</th>
							<th>Address</th>
							<th>Hep B Waiver</th>
							<th>Phone Number</th>
							<th>Prof License number</th>
              <th>Prof License Expiration</th>
              <th>Liability Insurance Exp</th>
              <th>CPR Exp</th>
              <th>Driver's License Exp</th>
              <th>Auto Insurance Exp</th>
              <th>Physical Exam Exp</th>
              <th>Flu Vaccine Exp</th>
              <th>Hep B Waiver</th>
              <th>PPD Exp</th>
              <th>Chest-Xray Exp</th>
              <th>Annual Evaluation</th>
              <th>Handwashing Competency</th>
              <th>Nursing Bag Technique Competency</th>
						</tr>
					</thead>
					<tbody>

						@foreach ($employee as $employees)
							<tr>
								<td>{{$employees->title}}</td>
								<td>{{$employees->dateOfHire}}</td>
								<td>{{$employees->name}}</td>
								<td>{{$employees->address}}</td>
								<td>{{$employees->hepBWaiver}}</td>
								<td>{{$employees->phoneNumber}}</td>
								<td>{{$employees->licenseNumber}}</td>
								<td>{{$employees->pl}}</td>
								<td>{{$employees->li}}</td>
								<td>{{$employees->c}}</td>
								<td>{{$employees->dl}}</td>
								<td>{{$employees->ai}}</td>
								<td>{{$employees->pe}}</td>
								<td>{{$employees->fv}}</td>
								<td>{{$employees->hepBWaiver}}</td>
								<td>{{$employees->p}}</td>
								<td>{{$employees->cx}}</td>
								<td>{{$employees->ae}}</td>
								<td>{{$employees->hwc}}</td>
								<td>{{$employees->nbtc}}</td>
							</tr>

						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endSection
@section('scripts')
	  <script src= '{{asset('js/admin/adminStatistics.js')}}'></script>
@stop
