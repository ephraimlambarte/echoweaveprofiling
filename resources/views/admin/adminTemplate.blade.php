<!DOCTYPE html>
<html lang="en">
  <head>
    @include('Partials._metaHead')

    @include('Partials.AdminPartials._adminStylesheet')
    @yield('stylesheet')
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-user"></i> <span>Hello Admin!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <!-- profile image here-->
                <img src="{{asset('images-database/admin/'.Auth::guard('admin')->id().'.'.session()->get('admin')['imageType'])}}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{session()->get('admin')['userName']}}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            @include('Partials.AdminPartials._sideBarAdmin')
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('logout.admin')}}">
                <!-- logout link here -->
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="{{asset('images-database/admin/'.Auth::guard('admin')->id().'.'.session()->get('admin')['imageType'])}}" alt="">{{session()->get('admin')['userName']}}
                    <!-- admin image here -->
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li id = 'adminProfile'><a href="{{route('admin.profile')}}">Profile</a></li>
                    <!--li><a href="javascript:;">Help</a></li-->
                    <li><a href="{{route('logout.admin')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    <!--Logout link here-->
                  </ul>
                </li>

                @include('Partials.AdminPartials._notifs')

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" style = 'overflow-x:scroll;'>
          @yield('content')
        </div>
        @include('Partials._footer')
        </div>
    </div>
      <!-- jQuery -->
    @include('Partials.AdminPartials._adminScripts')
    @yield('scripts')
  </body>
</html>
