@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/viewEmployeeDelete.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    <div class='row'>
      <div class='col-md-8'>
        <div class='well'>
          @include('Partials._message')
          {!! Form::open(['route' => 'employee.delete',
                  'data-parsley-validate'=>'',
                  'files'=>true]) !!}
              <div>
                <center>
                  <img src="{{asset('images-database/employee/'.$id.'.'.$imageType.'')}}" id = 'profileImage' class="genericImageCircle" alt="Cinque Terre">
                </center>
              </div>
              <div>
                {{Form::label('uploadProfileImage', 'Upload Profile Picture:')}}
                {{Form::file('uploadProfileImage', array('accept'=>
                  '.png,.jpg',
                  'disabled'=>''
                ))}}
              </div>
              {{Form::hidden('id', $id)}}
              {{Form::label('name', 'Full Name:')}}
              {{Form:: text('name', $name, array(
                                'placeholder'=>'Lastname, Firstname, M.I.',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                              'disabled'=>''))}}
              {{Form::label('address', 'Address:')}}
              {{Form:: text('address', $address, array(
                                'placeholder'=>'Street, Barangay, City',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                              'disabled'=>''))}}
              {{Form::label('hepBWaiver', 'Hep B Waiver:')}}
              {{Form:: text('hepBWaiver', $hepBWaiver, array(
                                'placeholder'=>'Hep B Waiver',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                              'disabled'=>''))}}
              {{Form::label('phoneNumber', 'Phone Number:')}}
              {{Form:: text('phoneNumber', $phoneNumber, array(
                                'placeholder'=>'####-###-####',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                              'disabled'=>''))}}
              {{Form::submit('Confirm Delete', array('class'=>'btn btn-danger btn-block',
                                  'style'=>'margin-top:20px;',

                                   'id'=>'saveEmployee'))}}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endSection
