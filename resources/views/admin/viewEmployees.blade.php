@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/viewEmployees.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
		<div class='row'>
			<div class='col-md-12'>
				@include('Partials._message')
				<table class='table talbe-striped'>
					<thead>
						<tr>
							<th>Name</th>
							<th>Address</th>
							<th>Hep B Waiver</th>
							<th>Phone Number</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($employee as $employees)
							@if($credentials[$employees->id])
								<tr class='bg-danger'>
									<td>{{$employees->name}}</td>
									<td>{{$employees->address}}</td>
									<td>{{$employees->hepBWaiver}}</td>
									<td>{{$employees->phoneNumber}}</td>
									<td><a href = {{route('employee.show', $employees->id)}} class='btn btn-sm btn-success'>Edit</a>
											<a href = {{route('employee.delete.confirm', $employees->id)}} class='btn btn-sm btn-danger'>Delete</a>
										  <a href = {{route('employee.credentials', $employees->id)}} class='btn btn-sm btn-info'>Credentials</a></td>
								</tr>
							@else
								<tr>
									<td>{{$employees->name}}</td>
									<td>{{$employees->address}}</td>
									<td>{{$employees->hepBWaiver}}</td>
									<td>{{$employees->phoneNumber}}</td>
									<td><a href = {{route('employee.show', $employees->id)}} class='btn btn-sm btn-success'>Edit</a>
											<a href = {{route('employee.delete.confirm', $employees->id)}} class='btn btn-sm btn-danger'>Delete</a>
										  <a href = {{route('employee.credentials', $employees->id)}} class='btn btn-sm btn-info'>Credentials</a></td>
								</tr>
							@endif
						@endforeach
					</tbody>
				</table>
				<div class='text-center'>
					{!!$employee->links()!!}
				</div>
			</div>
		</div>
	</div>
@endSection
