@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/insertEmployee.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    <div class='row'>
      <div class='col-md-8'>
        <div class='well'>
          @include('Partials._message')
          {!! Form::open(['route' => 'employee.update',
                  'data-parsley-validate'=>'',
                  'files'=>true]) !!}
              <div>
                <center>
                  <img src="{{asset('images-database/employee/'.$id.'.'.$imageType.'')}}" id = 'profileImage' class="genericImageCircle" alt="Cinque Terre">
                </center>
              </div>
              <div>
                {{Form::label('uploadProfileImage', 'Upload Profile Picture:')}}
                {{Form::file('uploadProfileImage', array('accept'=>
                  '.png,.jpg',
                  'disabled'=>''
                ))}}
              </div>
              {{Form::hidden('id', $id)}}
              {{Form::label('name', 'Full Name:')}}
              {{Form:: text('name', $name, array(
                                'placeholder'=>'Lastname, Firstname, M.I.',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                              'disabled'=>''))}}
              {{Form::label('address', 'Address:')}}
              {{Form:: text('address', $address, array(
                                'placeholder'=>'Street, Barangay, City',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                              'disabled'=>''))}}
              {{Form::label('hepBWaiver', 'Hep B Waiver:')}}
              {{Form:: text('hepBWaiver', $hepBWaiver, array(
                                'placeholder'=>'Hep B Waiver',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                              'disabled'=>''))}}
              {{Form::label('phoneNumber', 'Phone Number:')}}
              {{Form:: text('phoneNumber', $phoneNumber, array(
                                'placeholder'=>'####-###-####',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                              'disabled'=>''))}}
							{{Form::label('title', 'Title:')}}
							{{Form:: text('title', $title, array(
																'placeholder'=>'Title',
																'class'=>'form-control',
																'required'=>'',
																'maxlength'=>'100',
															'disabled'=>''))}}
							{{Form:: label('dateOfHire', 'Date Of Hire:')}}
						  {{Form::date('dateOfHire', $dateOfHire, array(
																									 'class'=>'input-sm form-control',
																									 'required'=>'',
																									 'disabled'=>''
																												 ))}}
              {{Form::submit('Save Employee', array('class'=>'btn btn-success btn-block',
                                  'style'=>'margin-top:20px;',
                                  'disabled'=>'',
                                   'id'=>'saveEmployee'))}}
          {!! Form::close() !!}
        </div>
        <button id = 'editButton' class='btn btn-primary'>Edit</button>
      </div>
    </div>
  </div>
@endSection
@section('scripts')
  <script src= '{{asset('js/parsley.min.js')}}'></script>
  <script src= '{{asset('js/admin/adminShow.js')}}'></script>
@stop
