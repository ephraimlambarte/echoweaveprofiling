@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/employeeCredentials.css')}}" rel="stylesheet">


@stop
@section('content')
  <div class='container'>
		<div class='row'>
			<div class='col-md-12'>
				<div class='row'>
					<div class='col-md-2'>
						<img class='myimage-md' src = {{asset('images-database/employee/'.$employee->id.'.'.$employee->imageType)}}>
					</div>
					<div class='col-md-3'>
						<h3>{{$employee->name}}'s Credentials</h3>
					</div>
				</div>
			</div>
		</div>
		{!! Form::open(['route' => 'employee.post.credentials',
						'data-parsley-validate'=>'',
						'files'=>true]) !!}
		<div class='row' style = 'margin-top:20px;'>
			@include('Partials._message')
			<div class='col-md-4'>

				<div class='well'>
					<h4>Liability Insurance</h4>
					<div class='well'>
							{{ Form::hidden('employeeId', $employee->id) }}
						  {{Form:: label('liabilityInsuranceStartDate', 'Start Date:')}}
							{{Form::date('liabilityInsuranceStartDate', $liabilityInsuranceStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('liabilityInsuranceEndDate', 'End Date:')}}
							{{Form::date('liabilityInsuranceEndDate', $liabilityInsuranceExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generateliabilityInsurance'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px; padding:10px;'>
							<div class='loader' id = 'liabilityInsuranceLoader'>

							</div>
						</div>
					</div>

						<center>
							<strong id = 'liabilityInsurancePercentage'>
								100%
							</strong>
						</center>
					</div>
			</div>

			<div class='col-md-4'>
				<div class='well'>
					<h4>Auto Insurance</h4>
					<div class='well'>
						  {{Form:: label('autoInsuranceStartDate', 'Start Date:')}}
							{{Form::date('autoInsuranceStartDate', $autoInsuranceStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('autoInsuranceEndDate', 'End Date:')}}
							{{Form::date('autoInsuranceEndDate', $autoInsuranceExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generateautoInsurance'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'autoInsuranceLoader'>

							</div>
						</div>
						<center>
							<strong id = 'autoInsurancePercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
		</div>
		<div class='col-md-4'>
			<div class='well'>
				<h4>Professional License</h4>
				<div class='well'>
						{{Form:: label('professionalLicenseNumber', 'License Number:')}}
						{{Form::text('professionalLicenseNumber', $profLicenseNumber, array(
																																							'class'=>'input-sm form-control'
																																										))}}
						{{Form:: label('professionalLicenseStartDate', 'Start Date:')}}
						{{Form::date('professionalLicenseStartDate', $profLicenseStartDate, array(
																																							'class'=>'input-sm form-control'
																																										))}}
						{{Form:: label('professionalLicenseEndDate', 'End Date:')}}
						{{Form::date('professionalLicenseEndDate', $profLicenseExpDate, array(
																																							'class'=>'input-sm form-control'
																																										))}}
						<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generateprofessionalLicense'>Generate</div>
				</div>
				<div>
					<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
						<div class='loader' id = 'professionalLicenseLoader'>

						</div>
					</div>
					<center>
						<strong id = 'professionalLicensePercentage'>
							100%
						</strong>
					</center>
				</div>
			</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-4'>
				<div class='well'>
					<h4>CPR</h4>
					<div class='well'>
						  {{Form:: label('cprStartDate', 'Start Date:')}}
							{{Form::date('cprStartDate', $cprStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('cprEndDate', 'End Date:')}}
							{{Form::date('cprEndDate', $cprExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generatecpr'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'cprLoader'>

							</div>
						</div>
						<center>
							<strong id = 'cprPercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
			</div>
			<div class='col-md-4'>
				<div class='well'>
					<h4>Driver's License</h4>
					<div class='well'>
						  {{Form:: label('driversLicenseStartDate', 'Start Date:')}}
							{{Form::date('driversLicenseStartDate', $driversLicenseStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('driversLicenseEndDate', 'End Date:')}}
							{{Form::date('driversLicenseEndDate', $driversLicenseExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generatedriversLicense'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'driversLicenseLoader'>

							</div>
						</div>
						<center>
							<strong id = 'driversLicensePercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
			</div>
			<div class='col-md-4'>
				<div class='well'>
					<h4>Physical Exam</h4>
					<div class='well'>
							{{Form:: label('physicalExamStartDate', 'Start Date:')}}
							{{Form::date('physicalExamStartDate', $physicalExamStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('physicalExamEndDate', 'End Date:')}}
							{{Form::date('physicalExamEndDate', $physicalExamExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generatephysicalExam'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'physicalExamLoader'>

							</div>
						</div>
						<center>
							<strong id = 'physicalExamPercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-4'>
				<div class='well'>
					<h4>Flu Vaccine</h4>
					<div class='well'>
							{{Form:: label('fluVaccineStartDate', 'Start Date:')}}
							{{Form::date('fluVaccineStartDate', $fluVaccineStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('fluVaccineEndDate', 'End Date:')}}
							{{Form::date('fluVaccineEndDate', $fluVaccineExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generatefluVaccine'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'fluVaccineLoader'>

							</div>
						</div>
						<center>
							<strong id = 'fluVaccinePercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
			</div>
			<div class='col-md-4'>
				<div class='well'>
					<h4>Chest X-Ray</h4>
					<div class='well'>
							{{Form:: label('chestXrayStartDate', 'Start Date:')}}
							{{Form::date('chestXrayStartDate', $chestXrayStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('chestXrayEndDate', 'End Date:')}}
							{{Form::date('chestXrayEndDate', $chestXrayExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generatechestXray'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'chestXrayLoader'>

							</div>
						</div>
						<center>
							<strong id = 'chestXrayPercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
			</div>
			<div class='col-md-4'>
				<div class='well'>
					<h4>Annual Evaluation</h4>
					<div class='well'>
							{{Form:: label('annualEvaluationStartDate', 'Start Date:')}}
							{{Form::date('annualEvaluationStartDate', $annualEvaluationStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('annualEvaluationEndDate', 'End Date:')}}
							{{Form::date('annualEvaluationEndDate', $annualEvaluationExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generateannualEvaluation'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'annualEvaluationLoader'>

							</div>
						</div>
						<center>
							<strong id = 'annualEvaluationPercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-4'>
				<div class='well'>
					<h4>Hand-Washing Competency</h4>
					<div class='well'>
							{{Form:: label('handWashingStartDate', 'Start Date:')}}
							{{Form::date('handWashingStartDate', $handWashingCompetencyStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('handWashingEndDate', 'End Date:')}}
							{{Form::date('handWashingEndDate', $handWashingCompetencyExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generatehandWashing'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'handWashingLoader'>

							</div>
						</div>
						<center>
							<strong id = 'handWashingPercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
			</div>
			<div class='col-md-4'>
				<div class='well'>
					<h4>Nursing Bag Technique Competency</h4>
					<div class='well'>
							{{Form:: label('nursingCompetencyStartDate', 'Start Date:')}}
							{{Form::date('nursingCompetencyStartDate', $nurBagTecCompStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('nursingCompetencyEndDate', 'End Date:')}}
							{{Form::date('nursingCompetencyEndDate', $nurBagTecCompExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generatenursingCompetency'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'nursingCompetencyLoader'>

							</div>
						</div>
						<center>
							<strong id = 'nursingCompetencyPercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
			</div>
			<div class='col-md-4'>
				<div class='well'>
					<h4>Competency Checklist</h4>
					<div class='well'>
							{{Form:: label('competencyChecklistStartDate', 'Start Date:')}}
							{{Form::date('competencyChecklistStartDate', $competencyStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('competencyChecklistEndDate', 'End Date:')}}
							{{Form::date('competencyChecklistEndDate', $competencyExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generatecompetencyChecklist'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'competencyChecklistLoader'>

							</div>
						</div>
						<center>
							<strong id = 'competencyChecklistPercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-md-4'>
				<div class='well'>
					<h4>PPD</h4>
					<div class='well'>
							{{Form:: label('ppdStartDate', 'Start Date:')}}
							{{Form::date('ppdStartDate', $ppdStartDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							{{Form:: label('ppdEndDate', 'End Date:')}}
							{{Form::date('ppdEndDate', $ppdExpDate, array(
																																								'class'=>'input-sm form-control'
																																											))}}
							<div class='btn btn-info btn-block btn-sm genericDivTop-sm' id = 'generateppd'>Generate</div>
					</div>
					<div>
						<div class='well' style = 'margin-bottom: 0px;padding:10px;'>
							<div class='loader' id = 'ppdLoader'>

							</div>
						</div>
						<center>
							<strong id = 'ppdPercentage'>
								100%
							</strong>
						</center>
					</div>
				</div>
			</div>
		</div>
				{{Form::submit('Submit Credentials', array('class'=>'btn btn-success btn-block',
														'style'=>'margin-top:20px;'))}}
			{!! Form::close() !!}
  </div>
@endSection

@section('scripts')
	<script src= '{{asset('js/parsley.min.js')}}'></script>
  <script type="text/javascript" src="{{asset('js/admin/employeeCredentials.js')}}"></script>

@stop
