@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/insertEmployee.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    <div class='row'>
      <div class='col-md-8'>
        <div class='well'>
          @include('Partials._message')
          {!! Form::open(['route' => 'view.employee.post',
                  'data-parsley-validate'=>'',
                  'files'=>true]) !!}
              <div>
                <center>
                  <img src="{{asset('images/defaultimage.png')}}" id = 'profileImage' class="genericImageCircle" alt="Cinque Terre">
                </center>
              </div>
              <div>
                {{Form::label('uploadProfileImage', 'Upload Profile Picture:')}}
                {{Form::file('uploadProfileImage', array('accept'=>
                  '.png,.jpg'
                ))}}
              </div>
              {{Form::label('name', 'Full Name:')}}
              {{Form:: text('name', null, array(
                                'placeholder'=>'Lastname, Firstname, M.I.',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100'))}}
              {{Form::label('address', 'Address:')}}
              {{Form:: text('address', null, array(
                                'placeholder'=>'Street, Barangay, City',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100'))}}
              {{Form::label('hepBWaiver', 'Hep B Waiver:')}}
              {{Form:: text('hepBWaiver', null, array(
                                'placeholder'=>'Hep B Waiver',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100'))}}
              {{Form::label('phoneNumber', 'Phone Number:')}}
              {{Form:: text('phoneNumber', null, array(
                                'placeholder'=>'####-###-####',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100'))}}
							{{Form::label('title', 'Title:')}}
							{{Form:: text('title', null, array(
																'placeholder'=>'Title',
																'class'=>'form-control',
																'required'=>'',
																'maxlength'=>'100'))}}
							{{Form:: label('dateOfHire', 'Date Of Hire:')}}
						  {{Form::date('dateOfHire', null, array(
																									 'class'=>'input-sm form-control',
																									 'required'=>''
																												 ))}}
              {{Form::submit('Save Employee', array('class'=>'btn btn-success btn-block',
                                  'style'=>'margin-top:20px;'))}}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endSection
@section('scripts')
  <script src= '{{asset('js/parsley.min.js')}}'></script>
    <script src= '{{asset('js/admin/adminInsert.js')}}'></script>
@stop
