@extends('admin.adminTemplate')

@section('title', 'AdminDashboard')
@section('stylesheet')
	<link href="{{asset('css/admin/adminProfile.css')}}" rel="stylesheet">
@stop
@section('content')
  <div class='container'>
    <div class='row'>
      <div class='col-md-8'>
        <div class='well'>
          @include('Partials._message')
          {!! Form::open(['route' => 'admin.profile.edit',
                  'data-parsley-validate'=>'',
                  'files'=>true]) !!}
              <div>
                <center>
                  <img src="{{asset('images-database/admin/'.Auth::guard('admin')->id().'.'.session()->get('admin')['imageType'].'')}}" id = 'profileImage' class="genericImageCircle" alt="Cinque Terre">
                </center>
              </div>
              <div>
              {{Form::label('uploadProfileImage', 'Upload Profile Picture:')}}
              {{Form::file('uploadProfileImage', array('accept'=>
                '.png,.jpg',
                'disabled'=>''
              ))}}
              </div>
              {{Form::hidden('id', Auth::guard('admin')->id())}}
              {{Form::label('userName', 'Username:')}}
              {{Form:: text('userName', session()->get('admin')['userName'], array(
                                'placeholder'=>'username',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                              'disabled'=>''))}}
              {{Form::label('email', 'Email:')}}
              {{Form:: text('email', session()->get('admin')['email'], array(
                                'placeholder'=>'email',
                                'class'=>'form-control',
                                'required'=>'',
                                'maxlength'=>'100',
                              'disabled'=>''))}}
              {{Form::submit('Save Admin', array('class'=>'btn btn-success btn-block',
                                  'style'=>'margin-top:20px;',
                                  'disabled'=>'',
                                   'id'=>'saveAdmin'))}}
          {!! Form::close() !!}
        </div>
        <button id = 'editButton' class='btn btn-primary'>Edit</button>
      </div>
    </div>
  </div>
@endSection
@section('scripts')
  <script src= '{{asset('js/parsley.min.js')}}'></script>
  <script src= '{{asset('js/admin/adminProfile.js')}}'></script>
@stop
