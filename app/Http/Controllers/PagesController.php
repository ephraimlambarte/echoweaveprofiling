<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function getLoginPage(){
      return view('admin/auth/adminAuthLoginPage');
    }
    public function getRegisterPage(){
      return view('admin/auth/adminRegisterPage');
    }
    public function getAdminHomePage(){
      return view('admin/adminHome');
    }
    public function getEmployeeView(){
      return view('admin/viewEmployees');
    }
    public function getEmployeeInsert(){
      return view('admin/employeeInsert');
    }
    public function showEmployee(){
      return view('admin/showInsertedEmployee');
    }
    public function getAdminProfile(){
      return view('admin/adminProfile');
    }
}
