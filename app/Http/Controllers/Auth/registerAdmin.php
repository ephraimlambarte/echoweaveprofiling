<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\admin;
use App\Rules\Adminkey;
use Session;
use Image;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Support\Str;
use App\Mail\adminMail;
class registerAdmin extends Controller
{
  private $request;
  private $admin;
  public function __construct(){
    $this->middleware('guest:admin');
    //$this->middleware('guest:admin', ['except'=>['logoutAdmin']]);
  }
  public function getRegisterPage(){
    if(admin::count()>0){
      return view('error', ['error'=>'Unauthorized Access']);
    }else{
      return view('admin/auth/adminRegisterPage');
    }
  }
  public function store(Request $request){
    //validate
    $this->validate($request, array(
                    'userName'=>'required|max:100|unique:admins,userName',
                    'password'=>'required|max:100|confirmed',
                    'email'=>'required|max:100|unique:admins,email|email',
                    'adminKey'=>['required', new Adminkey],
                    'uploadProfileImage' => 'required|image|mimes:jpeg,png,jpg|max:2048'
    ));

    //store to database

    $this->request = $request;
    $this->admin = new admin;
    DB::transaction(function() {
      $this->admin= new admin;
      $this->admin->userName = $this->request->userName;
      $this->admin->password = bcrypt($this->request->password);
      $this->admin->email = $this->request->email;
      $image = $this->request->file('uploadProfileImage');
      $this->admin->imageType = $image->getClientOriginalExtension();
      $this->admin->verify_token = Str::random(40);
      $this->admin->save();
      $filename = $this->admin->id.'.'.$image->getClientOriginalExtension();
      //error_log($filename);
      $location = public_path("images-database/admin/". $filename);
      Image::make($image)->resize(200,200)->save($location);
    });
    $this->verifyEmail(admin::find($this->admin->id));
    //save image to folder
    //redirect
    //return redirect()->route('admin.show', $this->admin->id);
    return redirect()->route('verify.page.admin', $this->admin->id);
  }
  public function getVerifyPage(){
    return view('admin/auth/emailVerification');
  }
  public function verifyEmail($thisAdmin){
    Mail::to($thisAdmin['email'])->send(new adminMail($thisAdmin));
  }

  public function sendEmailDone($email, $verifytoken){
    $admin = admin::where(['email'=>$email, 'verify_token'=>$verifytoken])->first();
    if($admin){
      admin::where(['email'=>$email, 'verify_token'=>$verifytoken])->update(['verified'=>'1', 'verify_token'=>null]);
      Session::flash('success', 'Admin registered!');
      return redirect()->route('show.created.admin', $admin['id']);
    }else{
      return 'user not found';
    }
  }
  public function show($id)
  {
      $admin = admin::find($id);
      return view('admin.auth.adminSuccessRegistration', [
                                                          'userName'=>$admin->userName,
                                                          'email' =>$admin->email
                                                        ]);
  }
}
