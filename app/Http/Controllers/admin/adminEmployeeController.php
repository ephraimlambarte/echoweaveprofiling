<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\employee;
use App\Model\liabilityInsurance;
use App\Model\profLicense;
use App\Model\cpr;
use App\Model\cprHist;
use App\Model\autoInsurance;
use App\Model\driversLicense;
use App\Model\physicalExam;
use App\Model\fluVaccine;
use App\Model\chestXray;
use App\Model\annualEvaluation;
use App\Model\handWashingCompetency;
use App\Model\nurBagTecComp;
use App\Model\competencyChecklist;
use App\Model\ppd;
use Illuminate\Support\Facades\DB;


use Mail;
use Illuminate\Support\Str;
use Session;
use Image;
class adminEmployeeController extends Controller
{
  private $request;
  private $employee;
  private $liabilityInsurance;
  private $profLicense;
  private $cpr;
  private $autoInsurance;
  private $driversLicense;
  private $physicalExam;
  private $fluVaccine;
  private $chestXray;
  private $annualEvaluation;
  private $handWashingCompetency;
  private $nurBagTecComp;
  private $competencyChecklist;
  private $ppd;
  public function __construct(){
    $this->middleware('auth:admin');
    //$this->middleware('guest:admin', ['except'=>['logoutAdmin']]);
  }
  public function getEmployeeCreatePage(){
    return view('admin/employeeInsert');
  }
  public function generateCsv(Request $request){
    $employee = DB::select(DB::raw('SELECT e.*, ae.expDate AS ae, ai.expDate
      AS ai, cx.expDate AS cx, dl.expDate AS dl, cc.expDate AS cc,
      c.expDate AS c, fv.expDate AS fv, hwc.expDate AS hwc, li.expDate
      AS li, nbtc.expDate AS nbtc, pe.expDate AS pe, p.expDate AS p,
      pl.profLicenseNumber AS licenseNumber, pl.expDate AS pl FROM
      employees e LEFT JOIN annual_evaluations ae ON ae.userID =
      e.id LEFT JOIN auto_insurances ai ON ai.userID = e.id LEFT
      JOIN chest_xrays cx ON cx.userID = e.id LEFT JOIN competency_checklists
      cc ON cc.userID = e.id LEFT JOIN cprs c ON c.userID =
      e.id LEFT JOIN drivers_licenses dl ON dl.userID = e.id
      LEFT JOIN flu_vaccines fv ON fv.userID = e.id LEFT
      JOIN hand_washing_competencies hwc ON hwc.userID = e.id
      LEFT JOIN liability_insurances li ON li.userID = e.id LEFT
      JOIN nur_bag_tec_comps nbtc ON nbtc.userID = e.id LEFT JOIN
      physical_exams pe ON pe.userID = e.id LEFT JOIN ppds p ON p.userID
     = e.id LEFT JOIN prof_licenses pl ON pl.userID = e.id GROUP BY e.id'));
  }
  public function displayEmployees(){
    $employees = employee::latest()->paginate(5);


    $employeecredential=false;

    foreach ($employees as $key) {
      if($this->checkEmployeeExpired($key->id)){
        $employeecredential[$key->id]=true;
      }else{
        $employeecredential[$key->id]=false;
      }
    }
    return view('admin/viewEmployees', ['employee'=>$employees,
                                        'credentials'=>$employeecredential]);
  }
  public function checkEmployeeExpired($employeeid){
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'liability_insurances'))== 0){
      return true;
    }
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'nur_bag_tec_comps'))== 0){
      return true;
    }
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'physical_exams'))== 0){
      return true;
    }
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'prof_licenses'))== 0){
      return true;
    }
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'hand_washing_competencies'))== 0){
      return true;
    }
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'flu_vaccines'))== 0){
      return true;
    }
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'cprs'))== 0){
      return true;
    }
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'competency_checklists'))== 0){
      return true;
    }
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'chest_xrays'))== 0){
      return true;
    }
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'auto_insurances'))== 0){
      return true;
    }
    if(count($this->checkEmployeeExpiredGeneric($employeeid, 'annual_evaluations'))== 0){
      return true;
    }
    return false;
  }
  private function checkEmployeeExpiredGeneric($employeeid, $table){
    return DB::table($table)->select()->where('userID', $employeeid)
    ->where('id', DB::table($table)->select('id')
    ->where('userID', $employeeid)->max('id'))
    ->whereRaw('DATEDIFF(expDate, NOW()) < 15')->get();
  }
  public function getDeletePage($employeeid){
    Session::flash('warning', 'Are you sure you want to delete?');
    $employee = employee::find($employeeid);
    return view('admin/viewEmployeeDelete', ['imageType'=>$employee->imageType,
                                               'id'=>$employeeid,
                                               'name'=>$employee->name,
                                               'address'=>$employee->address,
                                               'hepBWaiver'=>$employee->hepBWaiver,
                                               'phoneNumber'=>$employee->phoneNumber
                                              ]);
  }
  public function getCredentials($employeeid){
    $employee = employee::find($employeeid);
    $credentials = $this->employeeCredentials($employeeid);
    return view('admin/credentials/employeeCredentials', $credentials);
  }
  private function getGenericCredential($table, $employeeid, $date){
    if($date=='expDate'){
      return DB::table($table)->select('expDate')
      ->where('userID', $employeeid)->where('id', DB::table($table)->select('id')
      ->where('userID', $employeeid)->max('id'))->get()[0]->expDate;
    }else{
      return DB::table($table)->select('startDate')
      ->where('userID', $employeeid)->where('id', DB::table($table)->select('id')
      ->where('userID', $employeeid)->max('id'))->get()[0]->startDate;
    }
  }
  private function employeeCredentials($employeeid){
    $employee = employee::find($employeeid);
    try {
      $liabilityInsuranceExpDate = $this->getGenericCredential('liability_insurances', $employeeid, 'expDate');
      $liabilityInsuranceStartDate =$this->getGenericCredential('liability_insurances', $employeeid, 'startDate');
    } catch (\Exception $e) {
      //error_log($e->getMessage());
      $liabilityInsuranceExpDate = null;
      $liabilityInsuranceStartDate = null;
    }
    try {
      $ppdExpDate = $this->getGenericCredential('ppds', $employeeid, 'expDate');
      $ppdStartDate =$this->getGenericCredential('ppds', $employeeid, 'startDate');
    } catch (\Exception $e) {
      //error_log($e->getMessage());
      $ppdExpDate = null;
      $ppdStartDate = null;
    }

    try {
      $profLicenseExpDate = $this->getGenericCredential('prof_licenses', $employeeid, 'expDate');
      $profLicenseStartDate =$this->getGenericCredential('prof_licenses', $employeeid, 'startDate');
      $profLicenseNumber = profLicense::select('profLicenseNumber')
      ->where('userID', $employeeid)->where('id', liabilityInsurance::select('id')
      ->where('userID', $employeeid)->max('id'))->get()[0]->profLicenseNumber;

    }
    catch (\Exception $e) {
        $profLicenseExpDate = null;
        $profLicenseStartDate = null;
        $profLicenseNumber = null;
    }

    try {
      $cprExpDate =$this->getGenericCredential('cprs', $employeeid, 'expDate');
      $cprStartDate =$this->getGenericCredential('cprs', $employeeid, 'startDate');
    }
    catch (\Exception $e) {
        $cprExpDate = null;
        $cprStartDate = null;
    }

    try {
      $autoInsuranceExpDate =$this->getGenericCredential('auto_insurances', $employeeid, 'expDate');
      $autoInsuranceStartDate =$this->getGenericCredential('auto_insurances', $employeeid, 'startDate');
    }
    catch (\Exception $e) {
        $autoInsuranceExpDate = null;
        $autoInsuranceStartDate = null;
    }

    try {
      $driversLicenseExpDate =$this->getGenericCredential('drivers_licenses', $employeeid, 'expDate');
      $driversLicenseStartDate =$this->getGenericCredential('drivers_licenses', $employeeid, 'startDate');
    }
    catch (\Exception $e) {
        $driversLicenseExpDate = null;
        $driversLicenseStartDate = null;
    }


    try {
      $physicalExamExpDate = $this->getGenericCredential('physical_exams', $employeeid, 'expDate');
      $physicalExamStartDate =$this->getGenericCredential('physical_exams', $employeeid, 'startDate');
    }
    catch (\Exception $e) {
        $physicalExamExpDate = null;
        $physicalExamStartDate = null;
    }

    try {
      $fluVaccineExpDate = $this->getGenericCredential('flu_vaccines', $employeeid, 'expDate');
      $fluVaccineStartDate =$this->getGenericCredential('flu_vaccines', $employeeid, 'startDate');
    }
    catch (\Exception $e) {
        $fluVaccineExpDate = null;
        $fluVaccineStartDate = null;
    }

    try {
      $chestXrayExpDate = $this->getGenericCredential('chest_xrays', $employeeid, 'expDate');
      $chestXrayStartDate =$this->getGenericCredential('chest_xrays', $employeeid, 'startDate');
    }
    catch (\Exception $e) {
        $chestXrayExpDate = null;
        $chestXrayStartDate = null;
    }

    try {
      $annualEvaluationExpDate = $this->getGenericCredential('annual_evaluations', $employeeid, 'expDate');
      $annualEvaluationStartDate =$this->getGenericCredential('annual_evaluations', $employeeid, 'startDate');
    }
    catch (\Exception $e) {
        $annualEvaluationExpDate = null;
        $annualEvaluationStartDate = null;
    }
    try {
      $handWashingCompetencyExpDate = $this->getGenericCredential('hand_washing_competencies', $employeeid, 'expDate');
      $handWashingCompetencyStartDate =$this->getGenericCredential('hand_washing_competencies', $employeeid, 'startDate');
    }
    catch (\Exception $e) {
        $handWashingCompetencyExpDate = null;
        $handWashingCompetencyStartDate = null;
    }

    try {
      $nurBagTecCompExpDate = $this->getGenericCredential('nur_bag_tec_comps', $employeeid, 'expDate');
      $nurBagTecCompStartDate =$this->getGenericCredential('nur_bag_tec_comps', $employeeid, 'startDate');
    }
    catch (\Exception $e) {
        $nurBagTecCompExpDate = null;
        $nurBagTecCompStartDate = null;
    }

    try {
      $competencyExpDate = $this->getGenericCredential('competency_checklists', $employeeid, 'expDate');
      $competencyStartDate =$this->getGenericCredential('competency_checklists', $employeeid, 'startDate');
    }
    catch (\Exception $e) {
        $competencyExpDate = null;
        $competencyStartDate = null;
    }
    return array(
      'employee'=>$employee,
      'liabilityInsuranceExpDate'=> $liabilityInsuranceExpDate,
      'liabilityInsuranceStartDate' => $liabilityInsuranceStartDate,
      'profLicenseExpDate' => $profLicenseExpDate,
      'profLicenseStartDate' =>$profLicenseStartDate,
      'profLicenseNumber' =>$profLicenseNumber,
      'cprStartDate' => $cprStartDate,
      'cprExpDate' =>$cprExpDate,
      'autoInsuranceExpDate' => $autoInsuranceExpDate,
      'autoInsuranceStartDate' =>$autoInsuranceStartDate,
      'driversLicenseExpDate' =>$driversLicenseExpDate,
      'driversLicenseStartDate' =>$driversLicenseStartDate,
      'physicalExamStartDate' => $physicalExamStartDate,
      'physicalExamExpDate' =>$physicalExamExpDate,
      'fluVaccineExpDate' => $fluVaccineExpDate,
      'fluVaccineStartDate'=> $fluVaccineStartDate,
      'chestXrayExpDate' => $chestXrayExpDate,
      'chestXrayStartDate' =>$chestXrayStartDate,
      'annualEvaluationExpDate' => $annualEvaluationExpDate,
      'annualEvaluationStartDate' =>$annualEvaluationStartDate,
      'handWashingCompetencyExpDate' =>$handWashingCompetencyExpDate,
      'handWashingCompetencyStartDate' =>$handWashingCompetencyStartDate,
      'nurBagTecCompExpDate' => $nurBagTecCompExpDate,
      'nurBagTecCompStartDate' =>$nurBagTecCompStartDate,
      'competencyStartDate' => $competencyStartDate,
      'competencyExpDate' =>$competencyExpDate,
      'ppdStartDate' =>$ppdStartDate,
      'ppdExpDate' => $ppdExpDate
    );
  }
  public function postCredentials(Request $request){
    //validation
    if(!$this->checkCredential($request['liabilityInsuranceStartDate'],
    $request['liabilityInsuranceEndDate'], 'Liability Insurance')){
      return redirect()->back()->withInput();
    }
    if(!$this->checkCredential($request['autoInsuranceStartDate'],
    $request['autoInsuranceEndDate'], 'Auto Insurance')){
      return redirect()->back()->withInput();
    }
    if(!$this->checkCredential($request['autoInsuranceStartDate'],
    $request['autoInsuranceEndDate'], 'Auto Insurance')){
      return redirect()->back()->withInput();
    }

    if(!$this->checkCredential($request['ppdStartDate'],
    $request['ppdEndDate'], 'PPD')){
      return redirect()->back()->withInput();
    }

    if($request['professionalLicenseNumber']==null && ($request['professionalLicenseStartDate']!=null ||
                                                      $request['professionalLicenseEndDate']!=null)){
      if($this->checkCredential($request['professionalLicenseStartDate'],
      $request['professionalLicenseEndDate'], 'Professional License')){
        Session::flash('warning', 'A professional license number must be provided in Professional License');
        return redirect()->back()->withInput();
      }else{
        if($request['professionalLicenseStartDate'] ==null ^ $request['professionalLicenseEndDate']==null){
            //Session::flash('warning', 'A start date must have an end date or vice versa in '.$table);
            Session::forget('warning');
            Session::flash('warnings', ['A professional license number must be provided in Professional License',
                                       'A start date must have an end date or vice versa in Professional License']);
            return redirect()->back()->withInput();
        }else{
          if($request['professionalLicenseStartDate'] !=null && $request['professionalLicenseEndDate']!=null){
            if(strtotime($startdate)>strtotime($enddate)){
              //Session::flash('warning', 'A start date must be lesser in end date in '.$table);
              Session::forget('warning');
              Session::flash('warnings', ['A professional license number must be provided in Professional License',
                                         'A start date must be lesser in end date in Professional License']);
              return redirect()->back()->withInput();
            }
          }
        }
      }
    }else{
      if(!$this->checkCredential($request['professionalLicenseStartDate'],
      $request['professionalLicenseEndDate'], 'Professional License')){
        return redirect()->back()->withInput();
      }
      if($request['professionalLicenseNumber']!=null && ($request['professionalLicenseStartDate']==null ||
                                                        $request['professionalLicenseEndDate']==null)){
      Session::flash('warning', 'There must be a startdate and enddate for a license number in
                      Professional License');
      }
    }

    if(!$this->checkCredential($request['cprStartDate'],
    $request['cprEndDate'], 'CPR')){
      return redirect()->back()->withInput();
    }

    if(!$this->checkCredential($request['driversLicenseStartDate'],
    $request['driversLicenseEndDate'], "Driver's License")){
      return redirect()->back()->withInput();
    }

    if(!$this->checkCredential($request['physicalExamStartDate'],
    $request['physicalExamEndDate'], "Physical Exam")){
      return redirect()->back()->withInput();
    }

    if(!$this->checkCredential($request['fluVaccineStartDate'],
    $request['fluVaccineEndDate'], "Flu Vaccine")){
      return redirect()->back()->withInput();
    }

    if(!$this->checkCredential($request['chestXrayStartDate'],
    $request['chestXrayEndDate'], "Chest Xray")){
      return redirect()->back()->withInput();
    }

    if(!$this->checkCredential($request['annualEvaluationStartDate'],
    $request['annualEvaluationEndDate'], "Annual Evaluation")){
      return redirect()->back()->withInput();
    }

    if(!$this->checkCredential($request['handWashingStartDate'],
    $request['handWashingEndDate'], "Hand-Washing Competency")){
      return redirect()->back()->withInput();
    }

    if(!$this->checkCredential($request['nursingCompetencyStartDate'],
    $request['nursingCompetencyEndDate'], "Nursing Bag Technique Competency")){
      return redirect()->back()->withInput();
    }

    if(!$this->checkCredential($request['competencyChecklistStartDate'],
    $request['competencyChecklistEndDate'], "Competency Checklist")){
      return redirect()->back()->withInput();
    }
    //update to database
    $this->request = $request;
    $this->employee = new employee;

    DB::transaction(function() {
      if($this->request['liabilityInsuranceEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'liability_insurances')){
          $this->liabilityInsurance =  liabilityInsurance::find($this->getMaxIdCredential($this->request['employeeId'], 'liability_insurances'));
          $this->liabilityInsurance->expDate =$this->request['liabilityInsuranceEndDate'];
          $this->liabilityInsurance->startDate=$this->request['liabilityInsuranceStartDate'];

          $this->liabilityInsurance->save();
        }else{
          $this->liabilityInsurance = new liabilityInsurance;
          $this->liabilityInsurance->expDate =$this->request['liabilityInsuranceEndDate'];
          $this->liabilityInsurance->startDate=$this->request['liabilityInsuranceStartDate'];
          $this->liabilityInsurance->userID = $this->request['employeeId'];
          $this->liabilityInsurance->save();
        }
      }
      if($this->request['professionalLicenseEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'prof_licenses')){
          $this->profLicense = profLicense::find($this->getMaxIdCredential($this->request['employeeId'], 'prof_licenses'));
          $this->profLicense->profLicenseNumber= $this->request['professionalLicenseNumber'];
          $this->profLicense->expDate = $this->request['professionalLicenseEndDate'];
          $this->profLicense->startDate = $this->request['professionalLicenseStartDate'];
          $this->profLicense->save();
        }else{
          $this->profLicense = new profLicense;
          $this->profLicense->profLicenseNumber= $this->request['professionalLicenseNumber'];
          $this->profLicense->expDate = $this->request['professionalLicenseEndDate'];
          $this->profLicense->startDate = $this->request['professionalLicenseStartDate'];
          $this->profLicense->userID = $this->request['employeeId'];
          $this->profLicense->save();
        }
      }
      if($this->request['cprEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'cprs')){
          $this->cpr = cpr::find($this->getMaxIdCredential($this->request['employeeId'], 'cprs'));
          $this->cpr->expDate = $this->request['cprEndDate'];
          $this->cpr->startDate = $this->request['cprStartDate'];
          $this->cpr->save();
        }else{
          $this->cpr = new cpr;
          $this->cpr->expDate = $this->request['cprEndDate'];
          $this->cpr->startDate = $this->request['cprStartDate'];
          $this->cpr->userID = $this->request['employeeId'];
          $this->cpr->save();
        }
      }
      if($this->request['autoInsuranceEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'auto_insurances')){
          $this->autoInsurance = autoInsurance::find($this->getMaxIdCredential($this->request['employeeId'], 'auto_insurances'));
          $this->autoInsurance->expDate = $this->request['autoInsuranceEndDate'];
          $this->autoInsurance->startDate = $this->request['autoInsuranceStartDate'];
          $this->autoInsurance->save();
        }else{
          $this->autoInsurance = new autoInsurance;
          $this->autoInsurance->expDate = $this->request['autoInsuranceEndDate'];
          $this->autoInsurance->startDate = $this->request['autoInsuranceStartDate'];
          $this->autoInsurance->userID = $this->request['employeeId'];
          $this->autoInsurance->save();
        }
      }
      if($this->request['driversLicenseEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'drivers_licenses')){
          $this->driversLicense = driversLicense::find($this->getMaxIdCredential($this->request['employeeId'], 'drivers_licenses'));
          $this->driversLicense->expDate = $this->request['driversLicenseEndDate'];
          $this->driversLicense->startDate = $this->request['driversLicenseStartDate'];
          $this->driversLicense->save();
        }else{
          $this->driversLicense = new driversLicense;
          $this->driversLicense->expDate = $this->request['driversLicenseEndDate'];
          $this->driversLicense->startDate = $this->request['driversLicenseStartDate'];
          $this->driversLicense->userID = $this->request['employeeId'];
          $this->driversLicense->save();
        }
      }
      if($this->request['physicalExamEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'physical_exams')){
          $this->physicalExam = physicalExam::find($this->getMaxIdCredential($this->request['employeeId'], 'physical_exams'));
          $this->physicalExam->expDate = $this->request['physicalExamEndDate'];
          $this->physicalExam->startDate = $this->request['physicalExamStartDate'];
          $this->physicalExam->save();
        }else{
          $this->physicalExam = new physicalExam;
          $this->physicalExam->expDate = $this->request['physicalExamEndDate'];
          $this->physicalExam->startDate = $this->request['physicalExamStartDate'];
          $this->physicalExam->userID = $this->request['employeeId'];
          $this->physicalExam->save();
        }
      }

      if($this->request['fluVaccineEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'flu_vaccines')){
          $this->fluVaccine = fluVaccine::find($this->getMaxIdCredential($this->request['employeeId'], 'flu_vaccines'));
          $this->fluVaccine->expDate = $this->request['fluVaccineEndDate'];
          $this->fluVaccine->startDate = $this->request['fluVaccineStartDate'];
          $this->fluVaccine->save();
        }else{
          $this->fluVaccine = new fluVaccine;
          $this->fluVaccine->expDate = $this->request['fluVaccineEndDate'];
          $this->fluVaccine->startDate = $this->request['fluVaccineStartDate'];
          $this->fluVaccine->userID = $this->request['employeeId'];
          $this->fluVaccine->save();
        }
      }
      if($this->request['ppdEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'ppds')){
          $this->ppd = ppd::find($this->getMaxIdCredential($this->request['employeeId'], 'ppds'));
          $this->ppd->expDate = $this->request['ppdEndDate'];
          $this->ppd->startDate = $this->request['ppdStartDate'];
          $this->ppd->save();
        }else{
          $this->ppd = new ppd;
          $this->ppd->expDate = $this->request['ppdEndDate'];
          $this->ppd->startDate = $this->request['ppdStartDate'];
          $this->ppd->userID = $this->request['employeeId'];
          $this->ppd->save();
        }
      }

      if($this->request['chestXrayEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'chest_xrays')){
          $this->chestXray = chestXray::find($this->getMaxIdCredential($this->request['employeeId'], 'chest_xrays'));
          $this->chestXray->expDate = $this->request['chestXrayEndDate'];
          $this->chestXray->startDate = $this->request['chestXrayStartDate'];
          $this->chestXray->save();
        }else{
          $this->chestXray = new chestXray;
          $this->chestXray->expDate = $this->request['chestXrayEndDate'];
          $this->chestXray->startDate = $this->request['chestXrayStartDate'];
          $this->chestXray->userID = $this->request['employeeId'];
          $this->chestXray->save();
        }
      }

      if($this->request['annualEvaluationEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'annual_evaluations')){
          $this->annualEvaluation = annualEvaluation::find($this->getMaxIdCredential($this->request['employeeId'], 'annual_evaluations'));
          $this->annualEvaluation->expDate = $this->request['annualEvaluationEndDate'];
          $this->annualEvaluation->startDate = $this->request['annualEvaluationStartDate'];
          $this->annualEvaluation->save();
        }else{
          $this->annualEvaluation = new annualEvaluation;
          $this->annualEvaluation->expDate = $this->request['annualEvaluationEndDate'];
          $this->annualEvaluation->startDate = $this->request['annualEvaluationStartDate'];
          $this->annualEvaluation->userID = $this->request['employeeId'];
          $this->annualEvaluation->save();
        }
      }

      if($this->request['handWashingEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'hand_washing_competencies')){
          $this->handWashingCompetency = handWashingCompetency::find($this->getMaxIdCredential($this->request['employeeId'], 'hand_washing_competencies'));
          $this->handWashingCompetency->expDate = $this->request['handWashingEndDate'];
          $this->handWashingCompetency->startDate = $this->request['handWashingStartDate'];
          $this->handWashingCompetency->save();
        }else{
          $this->handWashingCompetency = new handWashingCompetency;
          $this->handWashingCompetency->expDate = $this->request['handWashingEndDate'];
          $this->handWashingCompetency->startDate = $this->request['handWashingStartDate'];
          $this->handWashingCompetency->userID = $this->request['employeeId'];
          $this->handWashingCompetency->save();
        }
      }

      if($this->request['nursingCompetencyEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'nur_bag_tec_comps')){
          $this->nurBagTecComp = nurBagTecComp::find($this->getMaxIdCredential($this->request['employeeId'], 'nur_bag_tec_comps'));
          $this->nurBagTecComp->expDate = $this->request['nursingCompetencyEndDate'];
          $this->nurBagTecComp->startDate = $this->request['nursingCompetencyStartDate'];
          $this->nurBagTecComp->save();
        }else{
          $this->nurBagTecComp = new nurBagTecComp;
          $this->nurBagTecComp->expDate = $this->request['nursingCompetencyEndDate'];
          $this->nurBagTecComp->startDate = $this->request['nursingCompetencyStartDate'];
          $this->nurBagTecComp->userID = $this->request['employeeId'];
          $this->nurBagTecComp->save();
        }
      }

      if($this->request['competencyChecklistEndDate']!= null){
        if($this->checkIfEmployeeHasCredential($this->request['employeeId'], 'competency_checklists')){
          $this->competencyChecklist = competencyChecklist::find($this->getMaxIdCredential($this->request['employeeId'], 'competency_checklists'));
          $this->competencyChecklist->expDate = $this->request['competencyChecklistEndDate'];
          $this->competencyChecklist->startDate = $this->request['competencyChecklistStartDate'];
          $this->competencyChecklist->save();
        }else{
          $this->competencyChecklist = new competencyChecklist;
          $this->competencyChecklist->expDate = $this->request['competencyChecklistEndDate'];
          $this->competencyChecklist->startDate = $this->request['competencyChecklistStartDate'];
          $this->competencyChecklist->userID = $this->request['employeeId'];
          $this->competencyChecklist->save();
        }
      }
    });
    Session::flash('success', 'Successfully saved!');
    return redirect()->route('employee.credentials', $request->employeeId);
  }
  private function checkIfEmployeeHasCredential($employeeid, $credentialtable){
    try{
      $id= DB::table($credentialtable)->select('id')->where('userID', $employeeid)
      ->where('id', DB::table($credentialtable)->select('id')->where('userID', $employeeid)->max('id'))->get()[0]->id;
    } catch (\Exception $e){
      return false;
    }
    return true;
  }
  private function getMaxIdCredential($employeeid, $table){
    return DB::table($table)->select('id')->where('userID', $employeeid)
    ->where('id', DB::table($table)->select('id')->where('userID', $employeeid)->max('id'))->get()[0]->id;
  }
  private function checkCredential($startdate, $enddate, $table){
    if($startdate ==null ^ $enddate==null){
        Session::flash('warning', 'A start date must have an end date or vice versa in '.$table);
        return false;
        //return redirect()->back()->withInput();
    }else{
      if($startdate !=null && $enddate!=null){
        if(strtotime($startdate)>strtotime($enddate)){
          Session::flash('warning', 'A start date must be lesser in end date in '.$table);
          return false;
          //return redirect()->back()->withInput();
        }
      }
    }
    return true;
  }
  public function getStatistics(){
    $employee = DB::select(DB::raw('SELECT e.*, ae.expDate AS ae, ai.expDate
      AS ai, cx.expDate AS cx, dl.expDate AS dl, cc.expDate AS cc,
      c.expDate AS c, fv.expDate AS fv, hwc.expDate AS hwc, li.expDate
      AS li, nbtc.expDate AS nbtc, pe.expDate AS pe, p.expDate AS p,
      pl.profLicenseNumber AS licenseNumber, pl.expDate AS pl FROM
      employees e LEFT JOIN annual_evaluations ae ON ae.userID =
      e.id LEFT JOIN auto_insurances ai ON ai.userID = e.id LEFT
      JOIN chest_xrays cx ON cx.userID = e.id LEFT JOIN competency_checklists
      cc ON cc.userID = e.id LEFT JOIN cprs c ON c.userID =
      e.id LEFT JOIN drivers_licenses dl ON dl.userID = e.id
      LEFT JOIN flu_vaccines fv ON fv.userID = e.id LEFT
      JOIN hand_washing_competencies hwc ON hwc.userID = e.id
      LEFT JOIN liability_insurances li ON li.userID = e.id LEFT
      JOIN nur_bag_tec_comps nbtc ON nbtc.userID = e.id LEFT JOIN
      physical_exams pe ON pe.userID = e.id LEFT JOIN ppds p ON p.userID
     = e.id LEFT JOIN prof_licenses pl ON pl.userID = e.id GROUP BY e.id'));

    return view('admin.statistics.statisticsView',['employee'=>$employee]);
  }
  public function deleteEmployee(Request $request){
    $employee = employee::find( $request->id);
    $employee->delete();
    Session::flash('success', 'Employee deleted.');
    return redirect()->route('employee.view');
  }

  public function storeEmployee(Request $request){
    //validate
    $this->validate($request, array(
                    'name'=>'required|max:192',
                    'address'=>'required|max:192',
                    'phoneNumber'=>'required|max:192',
                    'uploadProfileImage' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    'hepBWaiver'=>'required|max:192',
                    'title'=>'required|max:192',
                    'dateOfHire'=>'required'
    ));
    $this->request = $request;
    $this->employee = new employee;
    DB::transaction(function() {
      $this->employee->name = $this->request->name;
      $this->employee->address = $this->request->address;
      $this->employee->phoneNumber = $this->request->phoneNumber;
      $this->employee->hepBWaiver = $this->request->hepBWaiver;
      $this->employee->title = $this->request->title;
      $this->employee->dateOfHire = $this->request->dateOfHire;
      $image = $this->request->file('uploadProfileImage');
      $this->employee->imageType = $image->getClientOriginalExtension();

      $this->employee->save();
      $filename = $this->employee->id.'.'.$image->getClientOriginalExtension();
      //error_log($filename);
      $location = public_path("images-database/employee/". $filename);
      Image::make($image)->resize(200,200)->save($location);
    });
    Session::flash('success', 'employee saved!');
    return redirect()->route('employee.show', $this->employee->id);
  }
  public function showEmployee($employeeid){
    $employee = employee::find($employeeid);
    return view('admin/showInsertedEmployee', ['imageType'=>$employee->imageType,
                                               'id'=>$employeeid,
                                               'name'=>$employee->name,
                                               'address'=>$employee->address,
                                               'hepBWaiver'=>$employee->hepBWaiver,
                                               'phoneNumber'=>$employee->phoneNumber,
                                               'title'=>$employee->title,
                                               'dateOfHire'=>$employee->dateOfHire
                                              ]);
  }
  public function updateEmployee(Request $request){
    $this->validate($request, array(
                    'name'=>'required|max:192',
                    'address'=>'required|max:192',
                    'phoneNumber'=>'required|max:192',
                    'hepBWaiver'=>'required|max:192',
                    'dateOfHire'=>'required'
    ));

    $this->request = $request;
    $this->employee =  employee::find($request->id);
    DB::transaction(function() {
      $this->employee->name =   $this->request->name;
      $this->employee->address =   $this->request->address;
      $this->employee->phoneNumber =   $this->request->phoneNumber;
      $this->employee->hepBWaiver =   $this->request->hepBWaiver;
      $this->employee->dateOfHire = $this->request->dateOfHire;
      if($this->request->hasFile('uploadProfileImage')){
        $image = $this->request->file('uploadProfileImage');
        $this->employee->imageType =$image->getClientOriginalExtension();
        $filename = $this->employee->id.'.'.$image->getClientOriginalExtension();
        //error_log($filename);
        $location = public_path("images-database/employee/". $filename);
        Image::make($image)->resize(200,200)->save($location);
      }
      $this->employee->save();
    });
    Session::flash('success', 'employee updated!');
    return redirect()->route('employee.show', $this->employee->id);
  }
}
