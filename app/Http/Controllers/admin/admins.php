<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Model\admin;
use Auth;
use DB;
class admins extends Controller
{
  private $admin;
  private $request;
  public function __construct(){
    $this->middleware('auth:admin');
    //$this->middleware('guest:admin', ['except'=>['logoutAdmin']]);
  }
  public function getAdminHomePage(){
    return view('admin/adminHome');
  }
  public function getAdminProfile(){
    return view('admin/adminProfile');
  }
  public function editProfile(Request $request){
    $this->validate($request, array(
                    'userName'=>'required|max:100',
                    'email'=>'required|max:100|email',
                    'uploadProfileImage' => 'image|mimes:jpeg,png,jpg|max:2048'
    ));
    $this->admin = admin::find(Auth::guard('admin')->id());
    $this->request = $request;
    DB::transaction(function() {
      if($this->admin->userName != $this->request['userName']){
        $this->admin->userName = $this->request['userName'];
        $this->validate($this->request, array(
                        'userName'=>'unique:admins,userName',
        ));
      }
      if($this->admin->email != $this->request['email']){
        $this->admin->email = $this->request['email'];
        $this->validate($this->request, array(
                        'email'=>'unique:admins,email',
        ));
      }


      if($this->request->hasFile('uploadProfileImage')){
        $image = $this->request->file('uploadProfileImage');
        $this->admin->imageType=$image->getClientOriginalExtension();
        $filename = $this->admin->id.'.'.$image->getClientOriginalExtension();
        //error_log($filename);
        $location = public_path("images-database/admin/". $filename);
        Image::make($image)->resize(200,200)->save($location);
        $this->admin->save();
      }else{
        $this->admin->save();
      }
    });

    session()->get('admin')['userName']= $this->admin->userName;
    session()->get('admin')['email']=$this->admin->email;
    session()->get('admin')['imageType']=$this->admin->imageType;

    Session::flash('success', 'Profile updated!');
    return redirect()->route('admin.profile');
  }
}
