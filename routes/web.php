<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('admin')->group(function(){
  Route::get('/login', 'Auth\AdminLoginController@getAdminLoginPage')->name('login.admin');
  Route::post('/login/verify', 'Auth\AdminLoginController@login')->name('login.admin.verify');
  Route::get('/logout', 'Auth\AdminLoginController@logoutAdmin')->name('logout.admin');

  Route::get('/home', 'admin\admins@getAdminHomePage')->name('home.admin');

  Route::get('/register', 'Auth\registerAdmin@getRegisterPage')->name('register.admin');
  Route::post('/register/post', 'Auth\registerAdmin@store')->name('register.post.admin');
  Route::get('/register/show/{adminid}', 'Auth\registerAdmin@show')->name('show.created.admin');

  Route::get('/verify', 'Auth\registerAdmin@getVerifyPage')->name('verify.page.admin');
  Route::get('/verify/{email}/{verify_token}', 'Auth\registerAdmin@sendEmailDone')->name('verify.admin.email');

  //Route::get('/view/employee', 'PagesController@getEmployeeView')->name('view.employee');
  Route::get('/view/employee/insert', 'admin\adminEmployeeController@getEmployeeCreatePage')->name('view.employee.insert');
  Route::get('/employee/show/{employeeid}', 'admin\adminEmployeeController@showEmployee')->name('employee.show');
  Route::post('/employee/post', 'admin\adminEmployeeController@storeEmployee')->name('view.employee.post');
  Route::post('/employee/update', 'admin\adminEmployeeController@updateEmployee')->name('employee.update');
  Route::get('/employee/view', 'admin\adminEmployeeController@displayEmployees')->name('employee.view');
  Route::get('/employee/deleteconfirm/{employeeid}', 'admin\adminEmployeeController@getDeletePage')->name('employee.delete.confirm');
  Route::post('/employee/deleted/', 'admin\adminEmployeeController@deleteEmployee')->name('employee.delete');
  Route::get('/employeed/credentials/{employeeid}', 'admin\adminEmployeeController@getCredentials')->name('employee.credentials');
  Route::post('/employee/submit/credentials', 'admin\adminEmployeeController@postCredentials')->name('employee.post.credentials');
  ///Route::get('/employee/show/', 'PagesController@showEmployee')->name('employee.show');
  Route::get('/employee/statistics', 'admin\adminEmployeeController@getStatistics')->name('employee.statistics');
  Route::post('/employee/csv', 'admin\adminEmployeeController@generateCsv')->name('make.csv.file');

  Route::get('/profile', 'admin\admins@getAdminProfile')->name('admin.profile');
  Route::post('/profile/edit', 'admin\admins@editProfile')->name('admin.profile.edit');
});
