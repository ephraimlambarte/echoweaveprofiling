document.getElementById('uploadProfileImage').onchange = function(e) {
    // Get the first file in the FileList object
    var imageFile = this.files[0];
    // get a local URL representation of the image blob
    var url = window.URL.createObjectURL(imageFile);
    // Now use your newly created URL!
    var someImageTag = document.getElementById('profileImage');
    someImageTag.src = url;
}
document.getElementById('editButton').onclick = function(e){
  $("#uploadProfileImage").prop("disabled", false);
  $("#userName").prop("disabled", false);
  $("#email").prop("disabled", false);
  $("#saveAdmin").prop("disabled", false);

}
