$(document).ready(function(){
  setPercentage1($('#liabilityInsuranceStartDate').val(), $('#liabilityInsuranceEndDate').val(),
  'liabilityInsurancePercentage', 'liabilityInsuranceLoader');

  setPercentage1($('#professionalLicenseStartDate').val(), $('#professionalLicenseEndDate').val(),
  'professionalLicensePercentage', 'professionalLicenseLoader');

  setPercentage1($('#ppdStartDate').val(), $('#ppdEndate').val(),
  'ppdPercentage', 'ppdLoader');

  setPercentage1($('#autoInsuranceStartDate').val(), $('#autoInsuranceEndDate').val(),
  'autoInsurancePercentage', 'autoInsuranceLoader');

  setPercentage1($('#cprStartDate').val(), $('#cprEndDate').val(),
  'cprPercentage', 'cprLoader');

  setPercentage1($('#driversLicenseStartDate').val(), $('#driversLicenseEndDate').val(),
  'driversLicensePercentage', 'driversLicenseLoader');

  setPercentage1($('#physicalExamStartDate').val(), $('#physicalExamEndDate').val(),
  'physicalExamPercentage', 'physicalExamLoader');

  setPercentage1($('#fluVaccineStartDate').val(), $('#fluVaccineEndDate').val(),
  'fluVaccinePercentage', 'fluVaccineLoader');

  setPercentage1($('#chestXrayStartDate').val(), $('#chestXrayEndDate').val(),
  'chestXrayPercentage', 'chestXrayLoader');

  setPercentage1($('#annualEvaluationStartDate').val(), $('#annualEvaluationEndDate').val(),
  'annualEvaluationPercentage', 'annualEvaluationLoader');

  setPercentage1($('#handWashingStartDate').val(), $('#handWashingEndDate').val(),
  'handWashingPercentage', 'handWashingLoader');

  setPercentage1($('#nursingCompetencyStartDate').val(), $('#nursingCompetencyEndDate').val(),
  'nursingCompetencyPercentage', 'nursingCompetencyLoader');

  setPercentage1($('#competencyChecklistStartDate').val(), $('#competencyChecklistEndDate').val(),
  'competencyChecklistPercentage', 'competencyChecklistLoader');

  $("#generateliabilityInsurance").on('click', function(){
    setPercentage($('#liabilityInsuranceStartDate').val(), $('#liabilityInsuranceEndDate').val(),
                  'liabilityInsurancePercentage', 'liabilityInsuranceLoader');
  });

  $("#generateautoInsurance").on('click', function(){
    setPercentage($('#autoInsuranceStartDate').val(), $('#autoInsuranceEndDate').val(),
    'autoInsurancePercentage', 'autoInsuranceLoader');
  });

  $("#generateprofessionalLicense").on('click', function(){
    setPercentage($('#professionalLicenseStartDate').val(), $('#professionalLicenseEndDate').val(),
    'professionalLicensePercentage', 'professionalLicenseLoader');
  });
  $("#generateppd").on('click', function(){
    setPercentage1($('#ppdStartDate').val(), $('#ppdEndate').val(),
    'ppdPercentage', 'ppdLoader');
  });

  $("#generatecpr").on('click', function(){
    setPercentage($('#cprStartDate').val(), $('#cprEndDate').val(),
    'cprPercentage', 'cprLoader');
  });

  $("#generatedriversLicense").on('click', function(){
    setPercentage($('#driversLicenseStartDate').val(), $('#driversLicenseEndDate').val(),
    'driversLicensePercentage', 'driversLicenseLoader');
  });

  $("#generatephysicalExam").on('click', function(){
    setPercentage($('#physicalExamStartDate').val(), $('#physicalExamEndDate').val(),
    'physicalExamPercentage', 'physicalExamLoader');
  });

  $("#generatefluVaccine").on('click', function(){
    setPercentage($('#fluVaccineStartDate').val(), $('#fluVaccineEndDate').val(),
    'fluVaccinePercentage', 'fluVaccineLoader');
  });

  $("#generatechestXray").on('click', function(){
    setPercentage($('#chestXrayStartDate').val(), $('#chestXrayEndDate').val(),
    'chestXrayPercentage', 'chestXrayLoader');
  });

  $("#generateannualEvaluation").on('click', function(){
    setPercentage($('#annualEvaluationStartDate').val(), $('#annualEvaluationEndDate').val(),
    'annualEvaluationPercentage', 'annualEvaluationLoader');
  });

  $("#generatehandWashing").on('click', function(){
    setPercentage($('#handWashingStartDate').val(), $('#handWashingEndDate').val(),
    'handWashingPercentage', 'handWashingLoader');
  });

  $("#generatenursingCompetency").on('click', function(){
    setPercentage($('#nursingCompetencyStartDate').val(), $('#nursingCompetencyEndDate').val(),
    'nursingCompetencyPercentage', 'nursingCompetencyLoader');
  });

  $("#generatenursingCompetency").on('click', function(){
    setPercentage($('#competencyChecklistStartDate').val(), $('#competencyChecklistEndDate').val(),
    'competencyChecklistPercentage', 'competencyChecklistLoader');
  });
});
function setPercentage(startD, endD, percentageid, loaderid){
  //alert(startD);
  if(startD == '' || endD == ''){
    $("#"+loaderid).width('0');
    $("#"+percentageid).text('0%');
    //alert("Hello")
    return;
  }
  var today =  new Date();
  var thisdate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var startDate = new Date(startD);
    var endDate = new Date(endD);
    if (startDate>endDate){
      alert("Start date cannot be greater than end date!");
      $("#"+loaderid).width('0');
      $("#"+percentageid).text('0%');
      return;
    }

    var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    var todayDate =  new Date(thisdate);
    if(todayDate<startDate){
      $("#"+loaderid).width('0');
      $("#"+percentageid).text('0%');
      return;
    }
    if(todayDate>endDate){
      $("#"+loaderid).width('100%');
      $("#"+percentageid).text('100%');
      return;
    }
    var timeDiff2 = Math.abs(startDate.getTime() - todayDate.getTime());
    var diffDays2 = Math.ceil(timeDiff2 / (1000 * 3600 * 24));
    var percentage = (diffDays2/diffDays)*100;

    percentage = parseInt(percentage);
    $("#"+loaderid).width(percentage+'%');
    $("#"+percentageid).text(percentage+'%');
}

function setPercentage1(startD, endD, percentageid, loaderid){
  //alert(startD);
  if(startD == '' || endD == ''){
    $("#"+loaderid).width('0');
    $("#"+percentageid).text('0%');
    //alert("Hello")
    return;
  }
  var today =  new Date();
  var thisdate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var startDate = new Date(startD);
    var endDate = new Date(endD);
    if (startDate>endDate){
      $("#"+loaderid).width('0');
      $("#"+percentageid).text('0%');
      return;
    }

    var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    var todayDate =  new Date(thisdate);
    if(todayDate<startDate){
      $("#"+loaderid).width('0');
      $("#"+percentageid).text('0%');
      return;
    }
    if(todayDate>endDate){
      $("#"+loaderid).width('100%');
      $("#"+percentageid).text('100%');
      return;
    }
    var timeDiff2 = Math.abs(startDate.getTime() - todayDate.getTime());
    var diffDays2 = Math.ceil(timeDiff2 / (1000 * 3600 * 24));
    var percentage = (diffDays2/diffDays)*100;

    percentage = parseInt(percentage);
    $("#"+loaderid).width(percentage+'%');
    $("#"+percentageid).text(percentage+'%');
}
